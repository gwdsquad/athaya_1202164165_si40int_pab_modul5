package com.example.SC5redone;

public class Article {
    String author;
    String title;
    String body;
    String created_at;

    public Article(String author, String title, String body, String created_at) {
        this.author = author;
        this.title = title;
        this.body = body;
        this.created_at = created_at;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }
}
